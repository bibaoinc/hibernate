package com.bibao.util;

import org.hibernate.*;
import org.hibernate.cfg.*;

public class HibernateUtil {
	private static final SessionFactory FACTORY = buildSessionFactory();
	
	private static SessionFactory buildSessionFactory() {
		SessionFactory sessionFactory = null;
		try {
			sessionFactory = new Configuration().configure().buildSessionFactory();
		} catch (Exception e) {}
		return sessionFactory;
	}
	
	private static final ThreadLocal<Session> SESSION = new ThreadLocal<Session>() {
		@Override
		protected Session initialValue() {
			return FACTORY.openSession();
		}
	};
	
	public static SessionFactory getSessionFactory() {
		return FACTORY;
	}
	
	public static Session getCurrentSession() {
		Session session = SESSION.get();
		if (session==null) {
			session = FACTORY.openSession();
			SESSION.set(session);
		}
		return session;
	}
	
	public static void closeSession() {
		Session session = SESSION.get();
		SESSION.set(null);
		if (session!=null) session.close();
	}
}
